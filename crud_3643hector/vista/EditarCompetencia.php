<?php
require_once('../Modelo/Competencia.php');
require_once('../Modelo/CrudCompetencia.php');

$CrudCompetencia = new CrudCompetencia(); //crear un obeto crudcompetencia
$Competencia = $CrudCompetencia::ObtenerCompetencia($_GET['CodigoCompetencia']);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar</title>
</head>
<body>
    <h1 align="center">Editar Competencia</h1>
    <form action="../Controlador/ControladorCompetencia.php" method="POST">
        codigo Competencia:<input type="text" name="codigo_competencia" id="codigo_competencia"
        value="<?php echo $Competencia->getCodigoCompetencia(); ?>">
        <br>
        Nombre Competencia:<input type="text" name="nombre_competencia" id="nombre_competencia"
        value="<?php echo $Competencia->getNombreCompetencia(); ?>">
         <br>
        <input type="hidden" name="Editar">
        <button type="submit">Editar</button>
    </form>
</body>
</html>