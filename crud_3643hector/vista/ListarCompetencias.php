<?php
require_once('../Modelo/Competencia.php');
require_once('../Modelo/CrudCompetencia.php');

$CrudCompetencia = new CrudCompetencia();
$Competencia = new Competencia();
$ListaCompetencias = $CrudCompetencia::ListarCompetencias();// recibir datos de la consulta

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1 align="center">Listar Aprendices</h1>
<table align="center" border="1">
     <thead>
           <tr>
              <th>Codigo Competencia</th>
              <th>Nombre Competencia</th>
              <th>Acciones</th>
           </tr>  
     </thead>
     <tbody>
         <?php

           foreach($ListaCompetencias as $Competencia){
         ?>
            <tr>
                <td> 
                      <?php echo $Competencia->getCodigoCompetencia(); ?>
                </td>
                <td> 
                      <?php echo $Competencia->getNombreCompetencia(); ?>
                </td>
                <td>
                <a href="EditarCompetencia.php?CodigoCompetencia=<?php echo $Competencia->getCodigoCompetencia(); ?>">Editar</a>
                <a href="../Controlador/ControladorCompetencia.php?CodigoCompetencia=<?php echo $Competencia->getCodigoCompetencia(); ?>&Accion=EliminarCompetencia">Eliminar</a>
                </td>
            </tr>
         <?php
           }
         ?>
     </tbody>
</table>
  
</body>
</html>