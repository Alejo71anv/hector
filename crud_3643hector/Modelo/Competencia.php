<?php

class Competencia{
    //ATRIBUTOS
    private $CodigoCompetencia;
    private $NombreCompetencia;

    public function __construct(){

    }

    //set y get

    public function setCodigoCompetencia($CodigoCompetencia){
        $this->CodigoCompetencia = $CodigoCompetencia; 
    }

    public function getCodigoCompetencia(){
        return $this->CodigoCompetencia;
    }

    public function setNombreCompetencia($NombreCompetencia){
        $this->NombreCompetencia = $NombreCompetencia; 
    }

    public function getNombreCompetencia(){
        return $this->NombreCompetencia;
    }
}
/*
//testear la clase creando objetos yasignando valores a os atrubutos
$Competencia = new Competencia ();//definir la clase
$Competencia->setCodigoCompetencia(10); //asiganr valores atributos
$Competencia->setNombreCompetencia('Css'); //asiganr valores atributos

echo "codigo de la competencia: ".$Competencia->getCodigoCompetencia()."<br> Nombre competencia: ". $Competencia->getNombreCompetencia();

*/
?>