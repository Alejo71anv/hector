<?php
//codigo pra hacer la insercion desde el php
require_once('../Conexion.php');

class CrudCompetencia{
    //Constructor 
    public function __construct(){}

    public function InsertarCompetencia($Competencia){
        $Db = Db::conectar();
        $Sql = $Db->prepare('INSERT INTO competencias VALUES(:CodigoCompetencia,:NombreCompetencia)');
        $Sql->bindValue('CodigoCompetencia',$Competencia->getCodigoCompetencia());
        $Sql->bindValue('NombreCompetencia',$Competencia->getNombreCompetencia());//binvalue insertar
        //var_dump( $Sql);
        
        $Competencia->setNombrecompetencia('Intentando Hackear');
        
        try{
            $Sql->execute();//funcio propia de PDO y es para ejecutar el sql 
            echo "insercion exitosa";
        }
        catch(Exception $e){//para capturar el error al momento de la insercion
            echo $e->getMessage();
            die();
        }
    }

    public function ModificarCompetencia($Competencia){
        $Db = Db::conectar();
        //definir la modicacion a realizar
        $Sql = $Db->prepare('UPDATE competencias SET NombreCompetencia=:NombreCompetencia 
        WHERE CodigoCompetencia=:CodigoCompetencia');// el primer nombre de competencia es el q esta en la base de datos
        $Sql->bindValue('CodigoCompetencia',$Competencia->getCodigoCompetencia());
        $Sql->bindValue('NombreCompetencia',$Competencia->getNombreCompetencia());//binvalue insertar
        //var_dump( $Sql);
        
       // $Competencia->setNombrecompetencia('Intentando Hackear');
        
        try{
            $Sql->execute();//funcio propia de PDO y es para ejecutar el sql  Update
            echo "modificación exitosa";
        }
        catch(Exception $e){//para capturar el error al momento de la mODIFICACION
            echo $e->getMessage();
            die();
        }
    }

    public function EliminarCompetencia($CodigoCompetencia){
        $Db = Db::conectar();
        //definir la modicacion a realizar
        $Sql = $Db->prepare('DELETE FROM competencias WHERE CodigoCompetencia=:CodigoCompetencia');// el primer nombre de competencia es el q esta en la base de datos
        $Sql->bindValue('CodigoCompetencia',$CodigoCompetencia);
        
       // $Sql->bindValue('NombreCompetencia',$CpdigoCompetencia->getNombreCompetencia());//binvalue insertar
        //var_dump( $Sql);
        
       // $Competencia->setNombrecompetencia('Intentando Hackear');
        
        try{
            $Sql->execute();//funcio propia de PDO y es para ejecutar el sql QUE CONTIENE EL DELATE
            echo "Eliminacion exitosa";
        }
        catch(Exception $e){//para capturar el error al momento de la eliminacion
            echo $e->getMessage();
            die();
        }
    }

    
    public function ListarCompetencias(){
        //concetar a la DB
        $Db = Db::conectar();
        $ListaCompetencias = [];//arrays para almacenar datos de la consulta y retornarlos
         $Sql = $Db->query('SELECT * FROM competencias');//consultar query
        
         foreach($Sql->fetchALL() as $competencias){//fetchall es traer comportar como un array all lo q ay en todo ese array
          $MyCompetencia = new Competencia(); //crear un objetotipo competencia
          $MyCompetencia->setCodigoCompetencia($competencias['CodigoCompetencia']);
          $MyCompetencia->setNombreCompetencia($competencias['NombreCompetencia']);
          $ListaCompetencias[] = $MyCompetencia;
        }
        return $ListaCompetencias; //retorna el array con todos los obejetos de la consulta

    }

    public function ObtenerCompetencia($CodigoCompetencia){//esta funcon se va realizar la consulta ala tabla competencia //obtener una cmpetencia
        $Db = Db::conectar();
        $Sql = $Db->prepare('SELECT * FROM competencias WHERE CodigoCompetencia=:CodigoCompetencia');//perimte q no se vaya a realizar sql inyeccion se usa binvalue con dos puntos--- áa buscar por nombre se aplica AND nombrecompetencia
        $Sql->bindValue('CodigoCompetencia', $CodigoCompetencia);   
        $MyCompetencia = new Competencia();
        try{
            $Sql->execute();//funcio propia de PDO y es para ejecutar el sql ... ejecutar el uddate
            $Competencias = $Sql->fetch();//se almacena en la varibale $competencia los datos de las varibles $sql
            $MyCompetencia->setCodigoCompetencia($Competencias['CodigoCompetencia']);
            $MyCompetencia->setNombreCompetencia($Competencias['NombreCompetencia']);
        }
        catch(Exception $e){//para capturar el error al momento de la insercion
            echo $e->getMessage();//mostar errores en la notificacion
            die();
        }
        return $MyCompetencia;
    }

}   
?>